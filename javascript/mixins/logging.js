'use strict';

var Faye = require('../faye');

var Faye_Logging = {
  LOG_LEVELS: {
    fatal:  4,
    error:  3,
    warn:   2,
    info:   1,
    debug:  0
  },

  // `additionalData` is based off of the Raven additional data, https://docs.sentry.io/clients/node/usage/#raven-node-additional-data
  writeLog: function(message, additionalData, level) {
    // Only log on the given level if it's enabled
    if (!Faye.logger || (typeof Faye.logger[level] !== 'function' && typeof Faye.logger !== 'function')) return;

    var banner = '[Faye';
    var klass = this.className;

    for (var key in Faye) {
      if (klass) continue;
      if (typeof Faye[key] !== 'function') continue;
      if (this instanceof Faye[key]) klass = key;
    }
    if (klass) banner += '.' + klass;
    banner += '] ';

    if (typeof Faye.logger[level] === 'function')
      Faye.logger[level](banner + message, additionalData);
    else if (typeof Faye.logger === 'function')
      Faye.logger(banner + message, additionalData);
  }
};

(function() {
  for (var key in Faye_Logging.LOG_LEVELS)
    (function(level) {
      Faye_Logging[level] = function(message, additionalData) {
        this.writeLog(message, additionalData, level);
      };
    })(key);
})();

module.exports = Faye_Logging;
